import { Module } from '@nestjs/common';
import { TypeOrmModule } from "@nestjs/typeorm";
import { ProductsModule } from './products/products.module';
import { CategoriesModule } from './categories/categories.module';
import {
  KeycloakConnectModule,
  ResourceGuard,
  RoleGuard,
  AuthGuard,
  } from 'nest-keycloak-connect';
  import { APP_GUARD } from '@nestjs/core';

@Module({
  imports: [
    KeycloakConnectModule.register({
      authServerUrl: 'http://localhost:8080',
      realm: process.env.REALM_NAME,
      clientId: process.env.CLIENT_ID,
      secret: process.env.CLIENT_SECRET,
    }),
    TypeOrmModule.forRoot({ 
    type: "mysql",
    host: process.env.DB_HOST,
    port: 3307,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    autoLoadEntities: true,
    synchronize: true,
  }), 
  ProductsModule,
  CategoriesModule,
],
  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
      },
      {
      provide: APP_GUARD,
      useClass: ResourceGuard,
      },
      {
      provide: APP_GUARD,
      useClass: RoleGuard,
      },
  ],
})
export class AppModule {}

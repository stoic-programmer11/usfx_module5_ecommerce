import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import {
  Roles,
} from 'nest-keycloak-connect';

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Roles({ roles: ['admin_client_role'] })
  @Post()
  create(@Body() createProductDto: CreateProductDto) {
    return this.productsService.create(createProductDto);
  }

  @Roles({ roles: ['admin_client_role', 'user_client_role'] })
  @Get()
  findAll() {
    return this.productsService.findAll();
  }

  @Roles({ roles: ['admin_client_role', 'user_client_role'] })
  @Get(':id') 
  findOne(@Param('id') id: number) {
    return this.productsService.findOne(id);
  }

  @Roles({ roles: ['admin_client_role'] })
  @Patch(':id')
  update(@Param('id') id: number, @Body() updateProductDto: UpdateProductDto) {
    return this.productsService.update(id, updateProductDto);
  }

  @Roles({ roles: ['admin_client_role'] })
  @Delete(':id')
  remove(@Param('id') id: number) {
    return this.productsService.remove(id);
  }
}

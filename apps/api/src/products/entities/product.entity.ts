import { Category } from "src/categories/entities/category.entity";
import { Column, DeleteDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Product {

    //@PrimaryGeneratedColumn()
    @Column({primary: true, generated: true})
    id: number;

    @Column({ length: 100 })
    name: string;

    @Column('text')
    description: string;

    @Column('decimal', { precision: 10, scale: 2})
    price: number;

    @Column('int')
    quantity: number;

    @ManyToOne(() => Category, (category) => category.id,{
    eager: true, // to bring the category when making a findone
    }) 
    category: Category;

    @Column({ length: 255 })
    specsPdfUrl: string;

    @Column({ length: 255 })
    manualPdfUrl: string;

    @DeleteDateColumn()
    deleteAt: Date;
}

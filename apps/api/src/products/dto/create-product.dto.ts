import { IsString, MinLength, IsInt, IsPositive, IsNotEmpty, IsUrl, IsOptional } from "class-validator";

export class CreateProductDto {
    
    @IsString()
    @MinLength(2)
    name: string;

    @IsString()
    @MinLength(10)
    description: string;

    @IsPositive()
    price: number;

    @IsInt()
    @IsPositive()
    quantity: number;

    @IsNotEmpty()
    category: string;

    @IsUrl()
    @IsOptional() 
    specsPdfUrl: string;

    @IsUrl()
    @IsOptional() 
    manualPdfUrl: string;
}

import { Product } from "src/products/entities/product.entity";
import { Column, Entity, OneToMany } from "typeorm";

@Entity()
export class Category {
    
    @Column({primary: true, generated: true})
    id: number;

    @Column({ length: 100})
    name: string;

    @OneToMany(() => Product, product => product.category)
    products: Product[];
}

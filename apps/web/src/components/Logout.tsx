'use client'
// Logout.js
import React from "react";
import { TargetLogs } from "./TargetLogs";

export default function Logout() {
  return <TargetLogs buttonText="Sign out of Keycloak" authAction="signOut" />;
}


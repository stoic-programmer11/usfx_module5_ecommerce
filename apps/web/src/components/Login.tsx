'use client'
import React from "react";
import { TargetLogs } from "./TargetLogs";

export default function Login() {
  return <TargetLogs buttonText="Sign in with Keycloak" authAction="signIn" />;
}

import React from "react";
import { signIn, signOut } from "next-auth/react";

import logo from "../app/assets/logo.png";

interface TargetLogsProps {
  buttonText: string;
  authAction: "signIn" | "signOut";
}

export function TargetLogs({ buttonText, authAction }: TargetLogsProps) {
  const handleAuthAction = () => {
    if (authAction === "signIn") {
      signIn("keycloak");
    } else if (authAction === "signOut") {
      signOut({ callbackUrl: "/login" });
    }
  };

  return (
    <div className="target-logs-container">
      <div className="card">
        <img src={logo.src} alt="Logo" className="image" />
        <h1 className="text-2xl mb-4 text-center">
          {authAction === "signIn" ? "Welcome!" : "Goodbye!"}
        </h1>
        <button
          className={authAction === "signIn" ? "login-button" : "logout-button"}
          onClick={handleAuthAction}
        >
          {buttonText}
        </button>
      </div>
    </div>
  );
}

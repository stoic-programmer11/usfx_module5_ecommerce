import React from "react";
import profileImage from '../app/assets/profile.jpg';

const NavBarComp = () => {
  return (
    <div className="navbar">
      <div className="profile-container">
        <img
          src={profileImage.src}
          alt="Profile"
          className="profile-img"
        />
      </div>
      <div className="buttons">
        <button className="nav-btn">Home</button>
        <button className="nav-btn">Shopping Cart</button>
        <button className="nav-btn">Settings</button>
      </div>
      <button className="logout-btn">Logout</button>
    </div>
  );
};

export default NavBarComp;
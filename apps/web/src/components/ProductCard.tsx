'use client '
import React from "react";

interface ProductCardProps {
  productName: string;
  price: number;
  photoUrl: string;
}

const ProductCard: React.FC<ProductCardProps> = ({
  productName,
 
  price,
  photoUrl,
}) => {
  return (
    <div className="product-card">
      <img src={photoUrl} alt={productName} className="product-image" />
      <h2 className="product-name">{productName}</h2>

      <div className="product-info">
        <button className="add-to-cart-btn">Add to cart</button>
        <p className="product-price">{price.toFixed(2) } Bs.</p>
      </div>
    </div>
  );
};

export default ProductCard;

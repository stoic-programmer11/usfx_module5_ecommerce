// ProductGrid.tsx
import React from "react";
import ProductCard from "./ProductCard";

interface Product {
  id: number;
  productName: string;
  price: number;
  photoUrl: string;
}

interface ProductGridProps {
  products: Product[];
}

const ProductGrid: React.FC<ProductGridProps> = ({ products }) => {
  return (
    <div className="product-grid">
      {products.map((product) => (
        <ProductCard
          key={product.id}
          productName={product.productName}
          price={product.price}
          photoUrl={product.photoUrl}
        />
      ))}
    </div>
  );
};

export default ProductGrid;

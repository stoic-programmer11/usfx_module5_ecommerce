"use client";
import React from 'react';

export default function ManageAccountButton() {
  const handleAccountServiceRedirect = () => {
    window.location.href = 'http://localhost:8080/realms/e-commerce-usfx/account';
  };

  return (
    <button style={{ backgroundColor: 'blue', color: 'white', padding: '10px 20px', borderRadius: '5px', border: 'none', cursor: 'pointer' }} onClick={handleAccountServiceRedirect}>
        Manage Account
    </button>
  );
}
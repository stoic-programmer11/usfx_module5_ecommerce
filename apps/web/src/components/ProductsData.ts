// productsData.ts
export const products = [
    { id: 1, productName: "Ibuprofeno", sendCar: true, price: 20.00, photoUrl: "https://farmacorp.com/cdn/shop/products/7703763270620_590x590.jpg?v=1665682377" },
    { id: 2, productName: "Samsung Galaxy S7 FE", sendCar: true, price: 2499.00, photoUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQYDmA3cmvv-_2ALvoKEXJ-rK3cylX4Lw7hf4ggtbed3g&s'}, // Precio en Bs
    { id: 3, productName: "Vestido de Mujer Shein", sendCar: true, price: 150.00, photoUrl: "https://img.ltwebstatic.com/images3_pi/2021/07/20/1626790975622518406eb3eb9e62d84f4526f6ed2d_thumbnail_720x.webp" }, 
    { id: 4, productName: "Fideos Salteados a la Marina x PLATO", sendCar: false, price: 35.00, photoUrl: "https://mojo.generalmills.com/api/public/content/WTDAQTmskk2F3RaLYsF56A_gmi_hi_res_jpeg.jpeg?v=6c3b59fe&t=16e3ce250f244648bef28c5949fb99ff" }, 
    { id: 5, productName: "Samsung Galaxy Tab S6 Lite", sendCar: true, price: 1600.00, photoUrl: "https://www.cnet.com/a/img/resize/261225b1fb119c9ae9703c1f3390494e999944f8/hub/2020/06/29/930a5c48-a9cf-4956-b625-1d9bd178d163/samsung-galaxy-tab-s6-lite-0058.jpg?auto=webp&fit=crop&height=1200&width=1200" }, 
    { id: 6, productName: "Balón de Voley", sendCar: true, price: 100.00, photoUrl: "https://www.ecured.cu/images/c/cf/Balon-de-voleibol-8-1024x1024.jpg" }, 
    { id: 7, productName: "Protector Solar ISDIN", sendCar: true, price: 80.00, photoUrl: "https://protectorsolar.org/wp-content/uploads/2019/10/Analisis-del-protector-solar-Isdin.jpg" }, 
    { id: 8, productName: "DIGIVICE Tamagotchi", sendCar: true, price: 200.00, photoUrl: "https://i.redd.it/seqld0qjdpg71.jpg" }, 
    { id: 9, productName: "Perfume para Varón", sendCar: true, price: 250.00, photoUrl: "https://www.primor.eu/blog/wp-content/uploads/2023/10/Perfumes-franceses-para-hombre.jpg" } 
  ];
  
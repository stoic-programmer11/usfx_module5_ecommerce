// Importar tus componentes
import NavBarComp from "../components/NavBarComp";
import SearchComp from "../components/SearchComp";
import ProductGrid from "../components/ProductGrid";
import { getServerSession } from "next-auth";
import { authOptions } from "./api/auth/[...nextauth]/route";
import Login from "../components/Login";
import Logout from "../components/Logout";
import React = require("react");
import ManageAccount from "../components/ManageAccount"; 

<Login />;
export default async function Home() {
  const session = await getServerSession(authOptions);

  return (
    <div style={{ display: "flex" }}>
      {/* Contenedor del navbar */}
      <div>
        <NavBarComp />
      </div>
      {/* Contenedor del contenido principal */}
      <div style={{ marginLeft: "200px" }}>
        {/* Componente de búsqueda */}
        <div style={{ marginBottom: "20px" }}>
          <SearchComp />
        </div>
        {/* Grid de productos */}
        <div>
          {session ? (
            <div>
              <div>Your name is {session.user?.name}</div>
              <ProductGrid products={products} />
            </div>
          ) : (
            <Login />
          )}
        </div>
        <div>
          <ManageAccount />
        </div>
      </div>
    </div>
  );
}
